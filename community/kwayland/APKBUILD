# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-plasma
pkgname=kwayland
pkgver=6.2.0
pkgrel=0
pkgdesc="Qt-style Client and Server library wrapper for the Wayland libraries"
arch="all !armhf" # armhf blocked by extra-cmake-modules
url="https://www.kde.org"
license="LGPL-2.1-only OR LGPL-3.0-only"
depends_dev="
	qt6-qtwayland-dev
	wayland-dev
	"
makedepends="$depends_dev
	doxygen
	extra-cmake-modules
	plasma-wayland-protocols
	qt6-qtbase-dev
	qt6-qttools-dev
	wayland-protocols
	samurai
	"

case "$pkgver" in
	*.90*) _rel=unstable;;
	*) _rel=stable;;
esac
subpackages="$pkgname-dev $pkgname-doc $pkgname-dbg"
_repo_url="https://invent.kde.org/plasma/kwayland.git"
source="https://download.kde.org/$_rel/plasma/$pkgver/kwayland-$pkgver.tar.xz"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_QCH=ON
	cmake --build build
}

check() {
	ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
b914f3c9ff5ed8e2d56945b577585a5ffe18b4e25fa9832ff5ea61a166a83ee3ef27b1d1a0a0a77845d3587677977b68a86271dc5021e32303cc005befa7ee6e  kwayland-6.2.0.tar.xz
"
